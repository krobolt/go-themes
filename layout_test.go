package themes

import (
	"bytes"
	"fmt"
	"io"
	"testing"

	"github.com/stretchr/testify/assert"
)

type todo struct {
	Title string
	Done  bool
}

type todoPageData struct {
	PageTitle string
	Todos     []todo
}

func TestNewLayout(t *testing.T) {
	_, err := NewLayout("base", []string{"views/base.html"})
	assert.Nil(t, err)
}

func TestNewLayoutNoFile(t *testing.T) {
	_, err := NewLayout("base", []string{"views/foo.html"})
	assert.NotNil(t, err)
}

func TestAddFile(t *testing.T) {
	layout, _ := NewLayout("base", []string{"views/base.html"})
	ok, err := layout.AddFile("views/body.html")
	expected := []string{
		"views/base.html",
		"views/body.html",
	}
	actual := layout.Files()
	assert.Equal(t, expected, actual)
	assert.Equal(t, true, ok)
	assert.Nil(t, err)
}

func TestAddFileNotFound(t *testing.T) {
	layout, _ := NewLayout("base", []string{"views/base.html"})
	ok, err := layout.AddFile("views/none.html")
	assert.Equal(t, false, ok)
	assert.NotNil(t, err)
}

func TestClone(t *testing.T) {
	expectedName := "clone"
	layout, _ := NewLayout("base", []string{"views/base.html"})
	clone, err := layout.Clone("clone", []string{
		"views/body.html",
	})
	expected := []string{
		"views/base.html",
		"views/body.html",
	}
	actual := clone.Files()
	assert.Equal(t, expected, actual)
	assert.Equal(t, expectedName, clone.GetID())
	assert.Nil(t, err)
}

func TestCloneErr(t *testing.T) {
	layout, _ := NewLayout("base", []string{"views/base.html"})
	_, err := layout.Clone("clone", []string{
		"views/unknown.html",
	})
	assert.NotNil(t, err)
}

func TestCompileFreshCache(t *testing.T) {
	layout, _ := NewLayout("base", []string{"views/base.html"})
	expected := false
	actual, err := layout.Compile()
	assert.Equal(t, expected, actual)
	assert.Nil(t, err)
}

func TestCompileFreshStale(t *testing.T) {
	layout := &layout{
		id: "name",
		f:  []string{"views/base.html"},
		c:  &cache{},
	}
	expected := true
	actual, err := layout.Compile()
	assert.Equal(t, expected, actual)
	assert.Nil(t, err)
}

func TestCompileFreshStaleWithError(t *testing.T) {
	layout := &layout{
		id: "name",
		f:  []string{"views/unknown.html"},
		c:  &cache{},
	}
	expected := false
	actual, err := layout.Compile()
	assert.Equal(t, expected, actual)
	assert.NotNil(t, err)
}

func TestRenderWithError(t *testing.T) {
	_, w := io.Pipe()
	layout := &layout{
		id: "name",
		f:  []string{"views/unknown.html"},
		c:  &cache{},
	}
	data := getData()
	expected := false
	actual, err := layout.Render(w, data)
	w.Close()
	assert.Equal(t, expected, actual)
	assert.NotNil(t, err)

}

func TestRender(t *testing.T) {

	l, _ := NewLayout("test", []string{"views/base.html", "views/body.html"})

	r, w := io.Pipe()

	data := getData()

	go func() {
		l.Render(w, data)
		w.Close()
	}()

	buf := new(bytes.Buffer)
	buf.ReadFrom(r)
	fmt.Print(buf.String())

	expected := "<h1>My TODO list<h1><ul><li>Task 1</li><li>Task 2</li><li>Task 3</li></ul>"
	assert.Equal(t, buf.String(), expected)

}

func getData() todoPageData {
	return todoPageData{
		PageTitle: "My TODO list",
		Todos: []todo{
			{Title: "Task 1", Done: false},
			{Title: "Task 2", Done: true},
			{Title: "Task 3", Done: true},
		},
	}
}

func Test_createID(t *testing.T) {
	provider := []string{
		"test",
		"value",
	}
	expected := "1ac39c8b3ca51429753042dc7eff8611"
	actual := _createID(provider)
	assert.Equal(t, expected, actual)
}

func Test_inArrayTrue(t *testing.T) {
	provider := []string{
		"test",
		"value",
	}
	expected := true
	actual := _inArray(provider, "test")
	assert.Equal(t, expected, actual)
}
func Test_inArrayFalse(t *testing.T) {
	provider := []string{
		"test",
		"value",
	}
	expected := false
	actual := _inArray(provider, "unknown")
	assert.Equal(t, expected, actual)
}
