package themes

import (
	"crypto/md5"
	"encoding/hex"
	"html/template"
	"io"
	"os"
	"strings"
)

//Layout Interface Layouts must have a method for Render()
type Layout interface {
	Clone(id string, f []string) (Layout, error)
	GetID() string
	AddFile(s string) (bool, error)
	Files() []string
	Compile() (bool, error)
	Render(w io.Writer, data interface{}) (bool, error)
}

//Layout a Layout is a collection of templates
type layout struct {
	id string
	f  []string
	c  *cache
}

type cache struct {
	id string
	t  *template.Template
}

//NewLayout create New Layout
func NewLayout(name string, f []string) (Layout, error) {

	ok, err := _checkFiles(f)

	if !ok {
		return nil, err
	}

	l := &layout{
		id: name,
		f:  f,
		c:  &cache{},
	}

	_, err = l.Compile()
	return l, err

}

// Return layout Files
func (l *layout) AddFile(f string) (bool, error) {

	ok, err := _checkFiles([]string{f})

	if !ok {
		return ok, err
	}

	if !_inArray(l.Files(), f) {
		l.f = append(l.f, f)
	}

	return ok, err
}

//Clone Create new layout from exisiting files
func (l layout) Clone(id string, files []string) (Layout, error) {
	source := l.Files()
	c := source
	for _, f := range files {
		if !_inArray(source, f) {
			c = append(c, f)
		}
	}
	return NewLayout(id, c)
}

// Return layout Files
func (l *layout) Files() []string {
	return l.f
}

// Compile view and update cache
func (l *layout) Compile() (bool, error) {

	f := l.Files()
	id := _createID(f)

	if l.c.id == id {
		return false, nil
	}

	tmpl, err := template.ParseFiles(f...)
	if err != nil {
		return false, err
	}

	l.c = &cache{
		id: id,
		t:  tmpl,
	}
	return true, err
}

// Render view to io.Writer
func (l *layout) Render(w io.Writer, data interface{}) (bool, error) {
	state, err := l.Compile()
	if err != nil {
		return state, err
	}
	err = l.c.t.Execute(w, data)
	return state, err
}

// GetID Render view to io.Writer
func (l layout) GetID() string {
	return l.id
}

func _inArray(a []string, s string) bool {
	for _, v := range a {
		if s == v {
			return true
		}
	}
	return false
}

func _createID(f []string) string {
	data := strings.Join(f[:], ":")
	hasher := md5.New()
	hasher.Write([]byte(data))
	return hex.EncodeToString(hasher.Sum(nil))
}

func _checkFiles(f []string) (bool, error) {
	for _, v := range f {
		if _, err := os.Stat(v); err != nil {
			return false, err
		}
	}
	return true, nil
}
