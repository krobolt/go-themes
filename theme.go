package themes

//Theme A collection of layouts
type Theme interface {
	Add(l Layout) error
	Get(id string) (Layout, bool)
}

type theme struct {
	Layouts map[string]Layout
}

//NewTheme Return an empty named layout collection
func NewTheme() Theme {
	return &theme{
		Layouts: make(map[string]Layout),
	}
}

//Add layout to Theme
func (t *theme) Add(l Layout) error {
	t.Layouts[l.GetID()] = l
	return nil
}

//Get Layout from Theme
func (t *theme) Get(id string) (Layout, bool) {
	if t.Layouts[id] != nil {
		return t.Layouts[id], true
	}
	return nil, false
}
