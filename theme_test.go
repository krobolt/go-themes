package themes

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewTheme(t *testing.T) {
	expected := &theme{
		Layouts: make(map[string]Layout),
	}
	actual := NewTheme()
	assert.Equal(t, expected, actual)
}

func TestAdd(t *testing.T) {
	n := "layout"
	th := NewTheme()
	l, _ := NewLayout("layout", []string{})
	ly := make(map[string]Layout)
	ly[n] = l
	expected := &theme{
		Layouts: ly,
	}
	th.Add(l)
	actual := th
	assert.Equal(t, expected, actual)
}

func TestGet(t *testing.T) {
	th := NewTheme()
	l, _ := NewLayout("layout", []string{})
	th.Add(l)

	actual, ok := th.Get("layout")
	expected := l
	assert.Equal(t, expected, actual)
	assert.Equal(t, true, ok)
}

func TestGetMissing(t *testing.T) {
	th := NewTheme()

	actual, ok := th.Get("layout")
	assert.Nil(t, actual)
	assert.Equal(t, false, ok)
}
