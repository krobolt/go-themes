#Go Themes

Template theme management.

Example:

```	

	package main

	import (
		"bytes"
		"fmt"
		"io"
		"log"

		"gitlab.com/krobolt/go-themes"
	)

	type todo struct {
		Title string
		Done  bool
	}

	type todoPageData struct {
		PageTitle string
		Todos     []todo
	}

	func main() {
		r, w := io.Pipe()

		BaseLayout, err := themes.NewLayout("base", []string{
			"views/base.html",
		})

		if err != nil {
			log.Println(err)
		}

		DefaultTheme, err := BaseLayout.Clone("default", []string{
			"views/body.html",
		})

		Theme := themes.NewTheme()
		err = Theme.Add(DefaultTheme)

		layout, ok := Theme.Get("Admin")

		data := todoPageData{
			PageTitle: "My TODO list",
			Todos: []todo{
				{Title: "Task 1", Done: false},
				{Title: "Task 2", Done: true},
				{Title: "Task 3", Done: true},
			},
		}

		if ok {
			go func() {
				layout.Render(w, data)
				w.Close()
			}()
		}

		buf := new(bytes.Buffer)
		buf.ReadFrom(r)
		fmt.Print(buf.String())

	}

